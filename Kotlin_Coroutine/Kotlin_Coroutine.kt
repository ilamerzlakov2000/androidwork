import kotlin.concurrent.thread
import kotlin.random.Random

fun main() {
    var isPrinting = false

    // запускаем в фоновом потоке метод печати "World"
    thread {
        Thread.sleep(1000)
        println("World")
    }

    // запускаем печать "Hello," в основном потоке с задержкой в 2 секунды
    Thread.sleep(2000)
    // блокируем поток и изменяем значение isPrinting на true
    synchronized(isPrinting) {
        isPrinting = true
        println("Hello, ")
    }
    // ждем, пока фоновый поток закончит свою работу
    while (!isPrinting) {
        Thread.sleep(100)
    }
    // после того, как фоновый поток завершился, печатаем "The end"
    println("The end")
}
main()
println("________________________________________")

fun getNumber1(): Int {
    println("I'm tired of waiting!")
    var i = 1
    while (i < 4) {
        Thread.sleep(3000)
        println("I'm sleeping $i ...")
        i++
    }
    println("")
    return 5
}

fun getNumber2(): Int {
    var i = 1
    while (i < 3) {
    Thread.sleep(2000)
        println("I'm sleeping $i ...")
        i++
    }
    println("")
    return 10
}

fun main2() {
    val startTime = System.currentTimeMillis()

    val firstNumber = getNumber1()
    val secondNumber = getNumber2()

    val sum = firstNumber + secondNumber

    val endTime = System.currentTimeMillis()

    println("Sum: $sum")
    println("Time: ${endTime - startTime} ms.")
    println("")
    println("I'm running finally")
}
main2()

